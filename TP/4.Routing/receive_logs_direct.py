#!/usr/bin/env python
import pika
import sys
import uuid

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='direct_logs',
                         exchange_type='direct')


_uuid=str(uuid.uuid4())
result = channel.queue_declare(exclusive=True, queue=_uuid)
queue_name = result.method.queue

severities = sys.argv[1:]
if not severities:
    
    # print >> sys.stderr, "Usage: %s [info] [warning] [error]" % \
    #                      (sys.argv[0],)
    sys.exit(1)

for severity in severities:
    channel.queue_bind(exchange='direct_logs',
                       queue=queue_name,
                       routing_key=severity)

print (' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):
    print (" [x] %r:%r" % (method.routing_key, body,))

channel.basic_consume(queue_name, callback,
                      auto_ack=True)

channel.start_consuming()