python3 -m venv env
pip3 install pika

source env/bin/activate

docker build -t telecom/rabbitmq .
# Lancer avec bash
#docker run -it --rm -p 15672:15672 -p 5672:5672 telecom/rabbitmq /bin/bash
# Lancer sans bash
docker run -d --rm -p 15672:15672 -p 5672:5672 telecom/rabbitmq
